﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Actor
{
    public string Name;
    public GameObject Object;
}

public class Sequence : MonoBehaviour
{
    [Header("Sequence")]
    public bool Running = false;
    public Sequence FollowingSequence;

    [Header("Actors")]
    public List<Actor> Actors;
    protected Dictionary<string, GameObject> m_actorDictionary;


    protected virtual void Awake()
    {
        m_actorDictionary = new Dictionary<string, GameObject>();
        for (int i = 0; i < Actors.Count; i++)
        {
            m_actorDictionary.Add(Actors[i].Name, Actors[i].Object);
        }
    }

    protected virtual bool CanRunSequence()
    {
        return !Running;
    }

    public void TryRunSequence()
    {    
        if (CanRunSequence())
        {
            if (FollowingSequence != null)
            {
                GameManager.Instance.GetStateWithType<GameStateCinematic>(GameStateID.CINEMATIC).AddSequence(FollowingSequence);
            }

            Running = true;
            RunSequence();
        }
    }

    protected virtual void RunSequence()
    {
        StartCoroutine(BaseTimeline());
    }



    private IEnumerator BaseTimeline()
    {
        yield return PreTimeline();
        yield return Timeline();

        Running = false;
    }

    protected virtual IEnumerator PreTimeline()
    {
        yield return null;
    }

    protected virtual IEnumerator Timeline()
    {
        yield return null;
    }
}
