﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutscenePath : MonoBehaviour
{
    public List<Transform> Nodes;
    public float PreWaitTime = 0f;
    public float PostWaitTime = 0f;
    public float Duration = 1f;

    ///<summary>Returns true if this path has at least 3 Nodes, and the total Node count is divisible by 3.</summary>
    public bool IsValid { get { return Nodes != null && Nodes.Count > 2 && Nodes.Count % 3 == 0; } }


    ///<summary>Make sure that each Node triplet flows smoothly into the next one, so there won't be any jerky transitions.</summary>
    public void SmoothPath()
    {
        if (IsValid)
        {
            // smooth it out
            for (int i = 2; i + 2 < Nodes.Count; i += 2)
            {
                Nodes[i].position = Vector3.Lerp(Nodes[i - 1].position, Nodes[i + 1].position, 0.5f);
                Nodes[i].rotation = Quaternion.LookRotation(Vector3.Lerp(Nodes[i - 1].forward, Nodes[i + 1].forward, 0.5f), Vector3.Lerp(Nodes[i - 1].up, Nodes[i + 1].up, 0.5f));
            }
        }        
    }

    public void ReaddPath()
    {
        Nodes.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.name = "node" + i.ToString();
            Nodes.Add(transform.GetChild(i));
        }
    }



    private void OnDrawGizmos()
    {
        if (!IsValid)
            return;

        float step = 0f;
        Vector3 curr = new Vector3();
        Vector3 next = new Vector3();
        Vector3 forward = new Vector3();
        Vector3 up = new Vector3();
        for (int i = 0; i + 2 < Nodes.Count; i += 2)
        {
            while (step < 1f && Nodes[i] != null && Nodes[i + 1] != null && Nodes[i + 2] != null)
            {
                curr = HelperFunctions.QuadraticLerp(Nodes[i].position, Nodes[i + 1].position, Nodes[i + 2].position, step);
                step += 0.1f;
                next = HelperFunctions.QuadraticLerp(Nodes[i].position, Nodes[i + 1].position, Nodes[i + 2].position, step);

                forward = HelperFunctions.QuadraticLerp(Nodes[i].forward, Nodes[i + 1].forward, Nodes[i + 2].forward, step);
                up = HelperFunctions.QuadraticLerp(Nodes[i].up, Nodes[i + 1].up, Nodes[i + 2].up, step);

                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(next, next + forward * 5f);

                Gizmos.color = Color.magenta;
                Gizmos.DrawLine(next, next + up * 5f);

                Gizmos.color = Color.green;
                Gizmos.DrawLine(curr, next);
            }

            step = 0f;
        }
    }
}
