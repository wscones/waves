﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneSequence : Sequence
{
    [Space(20f)]
    [Header("Camera Paths")]
    public List<CutscenePath> Paths;
    public float PreTransitionDuration = 1f;
    public bool LerpToStart = true;

    private Transform m_cameraTransform;
    private Vector3 m_currentForward;
    private Vector3 m_currentUp;

    protected int m_currentIndex;
    protected float m_currentPercent;
    protected float m_totalDuration;


    protected override void Awake()
    {
        base.Awake();

        m_totalDuration += PreTransitionDuration;
        for (int i = 0; i < Paths.Count; i++)
        {
            if (Paths[i].Duration <= 0f)
            {
                Paths[i].Duration = 0.1f;
            }

            m_totalDuration += Paths[i].PreWaitTime;
            m_totalDuration += Paths[i].Duration;
            m_totalDuration += Paths[i].PostWaitTime;
        }
    }


    protected override bool CanRunSequence()
    {
        if (Paths.Count == 0)
            return false;

        // make sure all paths are valid
        for (int i = 0; i < Paths.Count; i++)
        {
            if (!Paths[i].IsValid)
            {
                return false;
            }
        }

        return base.CanRunSequence();
    }

    protected override void RunSequence()      
    {
        m_cameraTransform = GameManager.Instance.GetState(GameStateID.CINEMATIC).StateCamera.transform;
        base.RunSequence();
    }



    protected override IEnumerator PreTimeline()
    {
        m_currentIndex = 0;
        if (GameManager.Instance.PreviousStateID == GameStateID.OVERWORLD)
        {
            if (LerpToStart)
            {
                // lerp CutsceneCam from previous camera position to the first Node in the Cutscene
                GameManager.Instance.GetState(GameManager.Instance.PreviousStateID).SyncTargetToCamera(m_cameraTransform);
                yield return SyncCameraToTransform(PreTransitionDuration, Paths[0].Nodes[0]);
            }
            else
            {
                GameManager.Instance.GetState(GameStateID.CINEMATIC).SyncCameraToTarget(Paths[0].Nodes[0]);
                yield return null;
            }
        }
    }

    protected override IEnumerator Timeline()
    {
        for (int i = 0; i < Paths.Count; i++)
        {           
            yield return FollowSinglePath(Paths[i]);

            if (i + 1 < Paths.Count)
            {
                m_currentIndex++;
            }
        }

        GameManager.Instance.GetState(GameStateID.OVERWORLD).SyncCameraToTarget(m_cameraTransform);
    }


    private IEnumerator SyncCameraToTransform(float _duration, Transform _target)
    {
        float percent = 0f;
        float ratio = 1f / _duration;
        Vector3 startPos = m_cameraTransform.position;
        Vector3 startForward = m_cameraTransform.forward;
        while (percent < 1f)
        {
            percent += Time.deltaTime * ratio;
            m_cameraTransform.position = Vector3.Lerp(startPos, _target.position, percent);
            m_cameraTransform.forward = Vector3.Lerp(startForward, _target.forward, percent);

            yield return null;
        }
        percent = 0f;
    }

    private IEnumerator FollowSinglePath(CutscenePath _path)
    {
        // sync camera to first node in path
        m_cameraTransform.position = _path.Nodes[0].position;
        m_cameraTransform.forward = _path.Nodes[0].forward;

        if (_path.PreWaitTime > 0f)
        {
            yield return new WaitForSeconds(_path.PreWaitTime);
        }

        m_currentPercent = 0f;
        float ratio = 1f / _path.Duration;
        ratio *= (_path.Nodes.Count / 3);
        for (int i = 0; i + 2 < _path.Nodes.Count; i += 2)
        {
            while (m_currentPercent < 1f)
            {
                m_currentPercent += Time.deltaTime * ratio;
                m_cameraTransform.position = HelperFunctions.QuadraticLerp(_path.Nodes[i].position, _path.Nodes[i + 1].position, _path.Nodes[i + 2].position, m_currentPercent);
                m_currentForward = HelperFunctions.QuadraticLerp(_path.Nodes[i].forward, _path.Nodes[i + 1].forward, _path.Nodes[i + 2].forward, m_currentPercent);
                m_currentUp = HelperFunctions.QuadraticLerp(_path.Nodes[i].up, _path.Nodes[i + 1].up, _path.Nodes[i + 2].up, m_currentPercent);
                m_cameraTransform.rotation = Quaternion.LookRotation(m_currentForward, m_currentUp);

                yield return null;
            }
            m_currentPercent = 0f;
        }

        if (_path.PostWaitTime > 0f)
        {
            yield return new WaitForSeconds(_path.PostWaitTime);
        }
    }
}
