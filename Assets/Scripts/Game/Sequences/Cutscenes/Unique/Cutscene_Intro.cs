﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Cutscene_Intro : DialogueSequence
{
    Image fill;

    protected override IEnumerator PreTimeline()
    {
        if (fill == null)
        {
            fill = m_actorDictionary["fill"].GetComponent<Image>();
        }
        fill.enabled = true;

        yield return null;
    }

    protected override IEnumerator Timeline()
    {
        yield return base.Timeline();
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(Fade());
    }

    private IEnumerator Fade()
    {
       // Camera.
        //GameManager.Instance.GetState(GameStateID.OVERWORLD).SyncCameraToTarget(GameManager.Instance.GetState(GameStateID.CINEMATIC).StateCamera.transform);

        float percent = 0f;
        float ratio = 1f / (3f);
        while (percent < 1f)
        {
            percent += Time.deltaTime * ratio;
            fill.rectTransform.localScale = new Vector3(1f - percent, 1f - percent, 1f);
            //fill.color = Color.black * (1f - percent);
            yield return null;
        }
        fill.enabled = false;
    }
}
