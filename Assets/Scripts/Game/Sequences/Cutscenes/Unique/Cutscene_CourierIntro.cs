﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutscene_CourierIntro : CutsceneSequence
{
    GameObject courier;
    GameObject top;
    GameObject bottom;

    protected override IEnumerator PreTimeline()
    {
        courier = m_actorDictionary["mom"];
        top = m_actorDictionary["top"];
        bottom = m_actorDictionary["bot"];

        courier.transform.position = top.transform.position;

        yield return null;
    }

    protected override IEnumerator Timeline()
    {
        StartCoroutine(Descend());
        yield return base.Timeline();
    }

    private IEnumerator Descend()
    {     
        float percent = 0f;
        float ratio = 1f / (m_totalDuration);
        while (percent < 1f)
        {
            if (m_currentIndex == 1 && !courier.activeInHierarchy)
            {
                courier.SetActive(true);
            }

            percent += Time.deltaTime * ratio;
            courier.transform.position = Vector3.Lerp(top.transform.position, bottom.transform.position, percent);
            yield return null;
        }
    }
}
