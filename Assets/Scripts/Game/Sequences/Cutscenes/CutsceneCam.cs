﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CutsceneCam : MonoBehaviour 
{
    private List<Vector3> m_camPositions;
    private List<Quaternion> m_camRotations;
    private int m_currPosIndex;

    private float m_currSpeed;

    [SerializeField]
    protected Camera m_mainCamera;

    public virtual void Start()
    {
        m_camPositions = new List<Vector3>();
        m_camRotations = new List<Quaternion>();
        m_currPosIndex = 0;

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            m_camPositions.Add(gameObject.transform.GetChild(i).transform.position);
            m_camRotations.Add(gameObject.transform.GetChild(i).transform.rotation);
        }

        m_currSpeed = 0.03f;
    }

    public virtual void Update()
    {
    }

    protected IEnumerator followPath()
    {
        for (int i = 0; i < m_camPositions.Count; i++)
        {
            m_currSpeed = gameObject.transform.GetChild(i).gameObject.GetComponent<PathInfo>().PathSpeed;

            while (!HelperFunctions.ApproxEquals(gameObject.transform.position, m_camPositions[m_currPosIndex]))
            {
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, m_camPositions[m_currPosIndex], m_currSpeed);
                gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, m_camRotations[m_currPosIndex], 0.01f);
                yield return null;
            }

            m_currPosIndex++;
        }

        onCutsceneComplete();
    }

    protected virtual void onCutsceneComplete()
    {
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        for (int i = 0; i < gameObject.transform.childCount - 1; i++)
        {
            Gizmos.DrawLine(gameObject.transform.GetChild(i).transform.position, gameObject.transform.GetChild(i + 1).transform.position);
            Gizmos.DrawRay(gameObject.transform.GetChild(i).transform.position, gameObject.transform.GetChild(i).transform.forward * 10f);
        }
    }
}
