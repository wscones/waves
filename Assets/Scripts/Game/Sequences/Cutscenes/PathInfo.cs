﻿using UnityEngine;
using System.Collections;

public class PathInfo : MonoBehaviour 
{
    [SerializeField]
    private float m_pathSpeed = 0.03f;

    public float PathSpeed
    {
        get { return m_pathSpeed; }
    }
}
