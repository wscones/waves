﻿using System.Collections;
using UnityEngine;

public class DialogueSequence : Sequence
{
    [Space(20f)]
    [Header("Dialogue")]
    public SuperTextMesh SuperText;
    public string DialogueText;
    public float PreDelay = 0.2f;
    public float PostDelay = 0.2f;

    [Header("Camera")]
    public Transform Orientation;
    public float FOV = 60f;
    public float Speed = 0.2f;


    protected override IEnumerator Timeline()
    {
        yield return new WaitForSeconds(PreDelay);
        if (Orientation != null)
        {
            yield return GameManager.Instance.CameraManager.LerpTo(Orientation.position, Orientation.rotation, FOV, Speed);
        }

        string dialogue = "<c=cyan><j>" + DialogueText;
        SuperText.enabled = true;
        SuperText.text = dialogue;
        SuperText.Rebuild();
        yield return new WaitForEndOfFrame();

        SuperText.Read();
        while(SuperText.reading)
        {
            yield return null;
        }

        yield return new WaitForSeconds(PostDelay);
        SuperText.enabled = false;

        if (Orientation != null)
        {
            yield return GameManager.Instance.CameraManager.LerpToPreviousSettings(Speed);
        }
    }
}
