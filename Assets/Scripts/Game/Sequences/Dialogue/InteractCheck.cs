﻿using UnityEngine;

public class InteractCheck : MonoBehaviour
{
    public Sequence Dialogue;

    private void OnTriggerEnter(Collider _other)
    {
        if (_other.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.GetStateWithType<GameStateCinematic>(GameStateID.CINEMATIC).AddSequence(Dialogue);
            GameManager.Instance.GetState(GameStateID.OVERWORLD).SyncTargetToCamera(GameManager.Instance.GetState(GameStateID.CINEMATIC).StateCamera.transform);

            GameManager.Instance.GoToGameState(GameStateID.CINEMATIC);
        }
    }
}
