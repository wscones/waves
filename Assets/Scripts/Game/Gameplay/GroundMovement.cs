﻿using System;
using UnityEngine;

public class GroundMovement : MonoBehaviour
{
    private const string BUTTON_PICKUP = "PickUp";
    private const string AXIS_VERTICAL = "Vertical";
    private const string AXIS_HORIZONTAL = "Horizontal";

    private const string PARAM_ARMSUP = "armsUp";
    private const string PARAM_ISWALKING = "isWalking";

    [SerializeField]
    private Animator m_animator;

    public float MaxSpeed = 8f;


    private void Start()
    {
        GameManager.OnGameStateChanged += GameManager_OnGameStateChanged;
    }

    private void GameManager_OnGameStateChanged(GameStateID _stateID)
    {
        if (_stateID != GameStateID.OVERWORLD)
        {
            m_animator.SetBool(PARAM_ISWALKING, false);
        }
    }


    void Update()
    {
        if (GameManager.Instance.CurrentStateID == GameStateID.OVERWORLD)
        {
            HandleMovement();
            HandleRotation();
        }
    }

    private void HandleMovement()
    {
        float verticalInput = Input.GetAxis(AXIS_VERTICAL);
        if (verticalInput > 0.01f || verticalInput < -0.01f)
        {
            m_animator.SetBool(PARAM_ISWALKING, true);

            gameObject.transform.Translate(Vector3.forward * Time.deltaTime * Mathf.Sign(verticalInput) * HelperFunctions.ScaleClamp(Mathf.Sin(Time.time * 8f), -1f, 1f, MaxSpeed * 0.1f, MaxSpeed), Space.Self);
        }
        else
        {
            m_animator.SetBool(PARAM_ISWALKING, false);
        }
    }

    private void HandleRotation()
    {
        float horizontalInput = Input.GetAxis(AXIS_HORIZONTAL);
        if (horizontalInput > 0.01f || horizontalInput < -0.01f)
        {
            gameObject.transform.Rotate(Vector3.up, Time.deltaTime * 60f * Mathf.Sign(horizontalInput));
        }
    }
}
