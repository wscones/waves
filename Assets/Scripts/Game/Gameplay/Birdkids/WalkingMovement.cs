﻿using UnityEngine;

public class WalkingMovement : MonoBehaviour
{
    [SerializeField]
    private Animator m_animator;

    private bool m_isWalking;

    void Start()
    {
        m_animator.SetBool("isWalking", true);
        m_isWalking = true;

        InvokeRepeating("decide", 1f, 1f);
    }
	
	void FixedUpdate()
    {
        if (m_isWalking)
        {
            gameObject.transform.position += -gameObject.transform.right * 0.035f;
        }
	}

    private void decide()
    {
        if (Random.Range(0, 2) == 0)
        {
            m_isWalking = true;
            m_animator.SetBool("isWalking", true);
        }
        else
        {
            m_isWalking = false;
            m_animator.SetBool("isWalking", false);
        }
    }
}
