﻿using UnityEngine;

public class OffsetAnimationStart : MonoBehaviour 
{
	void Start () 
    {
        Invoke("startAnim", Random.Range(0f, 5f));
	}
	
	private void startAnim()
    {
        gameObject.GetComponent<Animator>().enabled = true;
    }
}
