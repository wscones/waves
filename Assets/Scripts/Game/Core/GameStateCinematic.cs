﻿using System.Collections;
using System.Collections.Generic;

public class GameStateCinematic : GameState
{
    private Queue<Sequence> m_sequenceQueue;


    override protected void Awake()
    {
        base.Awake();
        StateID = GameStateID.CINEMATIC;
        m_sequenceQueue = new Queue<Sequence>();
    }

    public override void Enter()
    {
        base.Enter();
        StartCoroutine(PlayQueue());
    }

    private IEnumerator PlayQueue()
    {
        while (m_sequenceQueue.Count > 0)
        {
            Sequence sequence = m_sequenceQueue.Dequeue();
            sequence.TryRunSequence();
            while (sequence.Running)
            {
                yield return null;
            }

            yield return null;
        }

        GameManager.Instance.GoToGameState(GameStateID.OVERWORLD);
    }

    public void AddSequence(Sequence _sequence)
    {
        m_sequenceQueue.Enqueue(_sequence);
    }
}
