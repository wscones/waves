﻿using UnityEngine;

public class WavesGameManager : GameManager
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            GoToGameState(GameStateID.CINEMATIC);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            GoToGameState(GameStateID.OVERWORLD);
        }
    }
}
