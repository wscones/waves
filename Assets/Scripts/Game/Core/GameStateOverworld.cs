﻿public class GameStateOverworld : GameState
{
    override protected void Awake()
    {
        base.Awake();
        StateID = GameStateID.OVERWORLD;
    }
}
