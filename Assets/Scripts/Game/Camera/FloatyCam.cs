﻿using System;
using System.Collections;
using UnityEngine;


public class FloatyCam : MonoBehaviour
{
    public float FollowDistance = 5f;
    public float FollowSpeed = 2f;
    public float LookSpeed = 2f;

    [SerializeField]
    private Transform m_target;
    private Vector3 m_lookAtPosition;

    //private bool m_isTweening;


    void Awake()
    {
        //m_isTweening = false;
    }



    void Update()
    {
        UpdateRotation();
        UpdatePosition();
    }    

    private void UpdateRotation()
    {
        m_lookAtPosition = Vector3.Lerp(m_lookAtPosition, m_target.position + m_target.forward * 2f, Time.deltaTime * LookSpeed);
        transform.LookAt(m_lookAtPosition);
    }

    private void UpdatePosition()
    {
        transform.position = Vector3.Lerp(transform.position, (m_target.position - m_target.forward * FollowDistance) + Vector3.down * 1f, Time.deltaTime * FollowSpeed);
    }
}
