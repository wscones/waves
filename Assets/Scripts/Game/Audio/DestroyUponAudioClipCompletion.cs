﻿using UnityEngine;

public class DestroyUponAudioClipCompletion : MonoBehaviour
{
    private AudioSource m_audioSource;


    public void Initialize(AudioClip _clip, Vector3 _position, float _volume)
    {
        transform.position = _position;
        gameObject.name = "AudioClip_" + _clip.name;

        m_audioSource = gameObject.AddComponent<AudioSource>();
        m_audioSource.clip = _clip;
        m_audioSource.volume = _volume;
        m_audioSource.spatialBlend = 1f;
        m_audioSource.Play();

        Invoke("Die", _clip.length);
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
