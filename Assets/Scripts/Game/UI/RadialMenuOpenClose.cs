﻿using UnityEngine;
using UnityEngine.UI;

public enum SlotEnum { None, Up, Down, Left, Right, Num_Slots };

public class RadialMenuOpenClose : MonoBehaviour
{
    private Image m_radialSlotUp, m_radialSlotDown, m_radialSlotLeft, m_radialSlotRight;
    private Vector3 m_slotUpPosition, m_slotDownPosition, m_slotLeftPosition, m_slotRightPosition;
    private Vector3 m_slotRestingScale;
    private Color m_transparentWhite;

    private SlotEnum m_slotSelected;

    public delegate void SlotDelegate(SlotEnum _slot);
    public static SlotDelegate OnSlotSelected;



    private void Start()
    {
        m_radialSlotUp = gameObject.transform.Find("radial_up").gameObject.GetComponent<Image>();
        m_radialSlotDown = gameObject.transform.Find("radial_down").gameObject.GetComponent<Image>();
        m_radialSlotLeft = gameObject.transform.Find("radial_left").gameObject.GetComponent<Image>();
        m_radialSlotRight = gameObject.transform.Find("radial_right").gameObject.GetComponent<Image>();

        m_slotUpPosition = Vector3.up * 100f;
        m_slotDownPosition = Vector3.up * -100f;
        m_slotLeftPosition = Vector3.right * -100f;
        m_slotRightPosition = Vector3.right * 100f;

        m_slotRestingScale = Vector3.one * 0.75f;
        m_transparentWhite = new Color(1f, 1f, 1f, 0f);

        m_slotSelected = SlotEnum.None;
    }



    private void Update()
    {
        openAndCloseMenu();   
    }

    private void openAndCloseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_slotSelected = SlotEnum.None;
             
            m_radialSlotUp.rectTransform.localPosition =
            m_radialSlotDown.rectTransform.localPosition =
            m_radialSlotLeft.rectTransform.localPosition =
            m_radialSlotRight.rectTransform.localPosition = Vector3.zero;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            if (m_slotSelected != SlotEnum.None && OnSlotSelected != null)
            {
                OnSlotSelected.Invoke(m_slotSelected);
            }
        }

        if (Input.GetKey(KeyCode.Space))
        {
            openMenu();
        }
        else
        {
            closeMenu();
        }
    }

    private void openMenu()
    {
        if (!HelperFunctions.ApproxEquals(m_radialSlotRight.color.a, 1f, 0.025f))
        {
            m_radialSlotUp.color = Color.Lerp(m_radialSlotUp.color, Color.white, Time.deltaTime * 16f);
            m_radialSlotDown.color = Color.Lerp(m_radialSlotDown.color, Color.white, Time.deltaTime * 16f);
            m_radialSlotLeft.color = Color.Lerp(m_radialSlotLeft.color, Color.white, Time.deltaTime * 16f);
            m_radialSlotRight.color = Color.Lerp(m_radialSlotRight.color, Color.white, Time.deltaTime * 16f);
        }
        else
        {
            if (!HelperFunctions.ApproxEquals(m_radialSlotUp.rectTransform.localPosition, m_slotUpPosition))
            {
                m_radialSlotUp.rectTransform.localPosition = Vector3.Lerp(m_radialSlotUp.rectTransform.localPosition, m_slotUpPosition, Time.deltaTime * 16f);
            }
            else
            {
                if (!HelperFunctions.ApproxEquals(m_radialSlotDown.rectTransform.localPosition, m_slotDownPosition))
                {
                    m_radialSlotDown.rectTransform.localPosition = Vector3.Lerp(m_radialSlotDown.rectTransform.localPosition, m_slotDownPosition, Time.deltaTime * 16f);
                }
                else
                {
                    if (!HelperFunctions.ApproxEquals(m_radialSlotLeft.rectTransform.localPosition, m_slotLeftPosition))
                    {
                        m_radialSlotLeft.rectTransform.localPosition = Vector3.Lerp(m_radialSlotLeft.rectTransform.localPosition, m_slotLeftPosition, Time.deltaTime * 16f);
                    }
                    else
                    {
                        if (!HelperFunctions.ApproxEquals(m_radialSlotRight.rectTransform.localPosition, m_slotRightPosition))
                        {
                            m_radialSlotRight.rectTransform.localPosition = Vector3.Lerp(m_radialSlotRight.rectTransform.localPosition, m_slotRightPosition, Time.deltaTime * 16f);
                        }
                        else
                        {
                            checkForSlotSelect();
                        }
                    }
                }
            }
        }
    }

    private void closeMenu()
    {
        m_slotSelected = SlotEnum.None;

        returnSlotToRest(m_radialSlotUp);
        returnSlotToRest(m_radialSlotDown);
        returnSlotToRest(m_radialSlotLeft);
        returnSlotToRest(m_radialSlotRight);
    }

    private void returnSlotToRest(Image _slot)
    {
        _slot.rectTransform.localPosition = Vector3.Lerp(_slot.rectTransform.localPosition, Vector3.zero, Time.deltaTime * 6f);
        _slot.rectTransform.localScale = Vector3.Lerp(_slot.rectTransform.localScale, m_slotRestingScale, Time.deltaTime * 6f);
        _slot.color = Color.Lerp(_slot.color, m_transparentWhite, Time.deltaTime * 6f);
    }



    private void checkForSlotSelect()
    {
        //input
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            m_slotSelected = SlotEnum.Up;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            m_slotSelected = SlotEnum.Down;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            m_slotSelected = SlotEnum.Left;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            m_slotSelected = SlotEnum.Right;
        }

        //increase scale of selected slot
        if (m_slotSelected == SlotEnum.Up)
        {
            m_radialSlotUp.rectTransform.localScale = Vector3.Lerp(m_radialSlotUp.rectTransform.localScale, m_slotRestingScale * 1.5f, 0.1f);
        }
        else if (m_slotSelected == SlotEnum.Down)
        {
            m_radialSlotDown.rectTransform.localScale = Vector3.Lerp(m_radialSlotDown.rectTransform.localScale, m_slotRestingScale * 1.5f, 0.1f);
        }
        else if (m_slotSelected == SlotEnum.Left)
        {
            m_radialSlotLeft.rectTransform.localScale = Vector3.Lerp(m_radialSlotLeft.rectTransform.localScale, m_slotRestingScale * 1.5f, 0.1f);
        }
        else if (m_slotSelected == SlotEnum.Right)
        {
            m_radialSlotRight.rectTransform.localScale = Vector3.Lerp(m_radialSlotRight.rectTransform.localScale, m_slotRestingScale * 1.5f, 0.1f);
        }

        //decrease scale of unselected slots
        if (m_slotSelected != SlotEnum.Up)
        {
            m_radialSlotUp.rectTransform.localScale = Vector3.Lerp(m_radialSlotUp.rectTransform.localScale, m_slotRestingScale, 0.1f);
        }
        if (m_slotSelected != SlotEnum.Down)
        {
            m_radialSlotDown.rectTransform.localScale = Vector3.Lerp(m_radialSlotDown.rectTransform.localScale, m_slotRestingScale, 0.1f);
        }
        if (m_slotSelected != SlotEnum.Left)
        {
            m_radialSlotLeft.rectTransform.localScale = Vector3.Lerp(m_radialSlotLeft.rectTransform.localScale, m_slotRestingScale, 0.1f);
        }
        if (m_slotSelected != SlotEnum.Right)
        {
            m_radialSlotRight.rectTransform.localScale = Vector3.Lerp(m_radialSlotRight.rectTransform.localScale, m_slotRestingScale, 0.1f);
        }
    }
}
