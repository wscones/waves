﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour
{
    public Transform Target;
    private Transform m_buffer;

	
    private void Update()
    {
        if (Target != null)
        {
            Vector3 to = Target.position - gameObject.transform.position;
            to.y = 0f;

            gameObject.transform.localRotation = Quaternion.LookRotation(to);
            gameObject.transform.Rotate(Vector2.up, 90f, Space.Self);
        }
    }
}
