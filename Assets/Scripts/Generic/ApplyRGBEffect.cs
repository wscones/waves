﻿using UnityEngine;

[ExecuteInEditMode]
public class ApplyRGBEffect : MonoBehaviour
{
    public float m_intensity;
    private Material m_pixelateMaterial;
    private Material m_cPrecisionMaterial;

    private RenderTexture m_intermediate;

    // Creates a private material used to the effect
    void Awake()
    {
        //m_cPrecisionMaterial = new Material(Shader.Find("Hidden/cPrecision"));
        m_pixelateMaterial = new Material(Shader.Find("Pixelate"));
    }

    void Start()
    {
    }

    // Postprocess the image
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (m_intensity == 0)
        {
            Graphics.Blit(source, destination);
            return;
        }

		Graphics.Blit(source, destination, m_pixelateMaterial);
        //m_intermediate = source;
        //Graphics.Blit(source, m_intermediate, m_cPrecisionMaterial);
        //Graphics.Blit(m_intermediate, destination, m_pixelateMaterial);
    }
}