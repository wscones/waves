﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Sequence))]
public class SequenceEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (Application.isPlaying)
        {
            EditorGUILayout.Space();
            if (GUILayout.Button("Add to Sequence Queue"))
            {
                WavesGameManager.Instance.GetStateWithType<GameStateCinematic>(GameStateID.CINEMATIC).AddSequence(target as Sequence);
                if (WavesGameManager.Instance.CurrentStateID != GameStateID.CINEMATIC)
                {
                    WavesGameManager.Instance.GoToGameState(GameStateID.CINEMATIC);
                }
            }
        }
    }
}