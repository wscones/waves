﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CutscenePath))]
public class CutscenePathEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CutscenePath script = (CutscenePath)target;
        if (GUILayout.Button("Smooth Path"))
        {
            script.SmoothPath();
        }
        if (GUILayout.Button("Re-add Path"))
        {
            script.ReaddPath();
        }
        
        GUILayout.Label("Is Valid: " + script.IsValid.ToString());        
    }
}