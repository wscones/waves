﻿using UnityEngine;
using UnityEditor;

public class CustomHotkeys : MonoBehaviour
{
    [MenuItem("Hotkeys/Align View to Selected %#d")]
    static void AlignViewToSelected()
    {
        EditorApplication.ExecuteMenuItem("GameObject/Align View to Selected");
    }
}
