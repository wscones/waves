﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(CutsceneSequence))]
public class CutsceneSequenceEditor : SequenceEditor
{
    string test;

    public int ActorCount = 0;
    public List<string> Keys;
    public List<GameObject> Values;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (!Application.isPlaying)
        {
            if (Keys == null)
                Keys = new List<string>();

            if (Values == null)
                Values = new List<GameObject>();

            GUILayout.Space(20f);
            for (int i = 0; i < Keys.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Actor " + i.ToString() + ":");
                Keys[i] = EditorGUILayout.TextField(Keys[i]);
                if (i < Values.Count)
                {
                    Values[i] = EditorGUILayout.ObjectField(Values[i], typeof(GameObject), true) as GameObject;
                }
                EditorGUILayout.EndHorizontal();
            }
        }
        else
        {
            //CutsceneSequence script = target as CutsceneSequence;
            //for (int i = 0; i < script.Actors.Count; i++)
            //{
            //    EditorGUILayout.BeginHorizontal();
            //    EditorGUILayout.LabelField(script.Actors[i].Name);
            //    EditorGUILayout.LabelField((script.Actors[i].Object == null ? "null" : script.Actors[i].Object.name));
            //    EditorGUILayout.EndHorizontal();
            //}      
        }
        //CutsceneSequence script = (CutsceneSequence)target;
        //GUILayout.BeginHorizontal();

        //test = "";
        //for (int i = 0; i < script.Paths.Count; i++)
        //{
        //    for (int j = 0; j < script.Paths[i].Time; j++)
        //    {
        //        test += "-";                
        //    }

        //    GUILayout.Label(test + (i < script.Paths.Count - 1 ? "|" : ""));
        //}
    }
}