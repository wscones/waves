﻿using UnityEngine;

public enum GameStateID
{
    NONE,
    MENU,
    OVERWORLD,
    CINEMATIC,
    NUM_STATES
}

public class GameState : MonoBehaviour
{
    public GameStateID StateID;
    public Camera StateCamera;

    protected virtual void Awake()
    {
        StateCamera.gameObject.SetActive(false);
    }

    public virtual void Enter()
    {
        GameManager.Instance.CameraManager.SetCamera(StateCamera);
        GameManager.Instance.Canvas.worldCamera = StateCamera;
    }
    public virtual void Exit()
    {
    }

    public void SyncTargetToCamera(Transform _target)
    {
        _target.position = StateCamera.transform.position;
        _target.rotation = StateCamera.transform.rotation;
    }

    public void SyncCameraToTarget(Transform _target)
    {
        StateCamera.transform.position = _target.position;
        StateCamera.transform.rotation = _target.rotation;
    }
}
