﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour 
{
    private Dictionary<string, AudioClip> m_audioClipDictionary;


	void Start() 
    {
        m_audioClipDictionary = new Dictionary<string, AudioClip>();

        AudioClip[] clips = Resources.LoadAll<AudioClip>("Audio");
        for (int i = 0; i < clips.Length; i++)
        {
            m_audioClipDictionary.Add(clips[i].name, clips[i]);
        }
	}

	
	public void PlaySoundAtPosition(string _clipName, Vector3 _position, float _volume)
    {
        GameObject soundObj = new GameObject();
        soundObj.transform.SetParent(transform);

        soundObj.AddComponent<DestroyUponAudioClipCompletion>();
        soundObj.GetComponent<DestroyUponAudioClipCompletion>().Initialize(m_audioClipDictionary[_clipName], _position, _volume);
    }
}
