﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class IntroFinishedEvent : UnityEvent<bool>
{
}

public class DisplayIntroTitle : UnityEvent<bool>
{
}

public class DisplayEndingEvent : UnityEvent<bool>
{
}

public class ShowEndingCutsceneEvent : UnityEvent<bool>
{
}

public class EventManager : MonoBehaviour
{
    public static IntroFinishedEvent IntroFinished;
    public static DisplayIntroTitle IntroTitle;
    public static DisplayEndingEvent DisplayEnding;
    public static ShowEndingCutsceneEvent ShowEndingCutscene;

    void Awake()
    {
        if (IntroFinished == null)
        {
            IntroFinished = new IntroFinishedEvent();
        }

        if (IntroTitle == null)
        {
            IntroTitle = new DisplayIntroTitle();
        }

        if (DisplayEnding == null)
        {
            DisplayEnding = new DisplayEndingEvent();
        }

        if (ShowEndingCutscene == null)
        {
            ShowEndingCutscene = new ShowEndingCutsceneEvent();
        }
    }

    void Update()
    {
    }
}