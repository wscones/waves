﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component
{
    private static object m_lock = new object();
    private static bool m_applicationIsQuitting = false;

    private static T m_instance = null;
    public static T Instance
    {
        get
        {
            if (m_applicationIsQuitting)
                return null;

            lock (m_lock)
            {
                if (m_instance == null)
                    m_instance = FindObjectOfType<T>();

                return m_instance;
            }
        }
    }


    protected virtual void OnDestroy()
    {
        m_applicationIsQuitting = true;
    }

    protected virtual void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this as T;
            DontDestroyOnLoad(gameObject);
        }
        else if (m_instance != this)
        {
            Destroy(gameObject);
        }
    }
}
