﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSettings
{
    public Vector3 Position;
    public Quaternion Rotation;
    public float FOV;
}

public class CameraManager : MonoBehaviour
{
    private Camera m_currentCamera;
    private Vector3 m_cameraDestination;
    private CameraSettings m_previousSettings;


    private void Start()
    {
        m_previousSettings = new CameraSettings();
    }

    public void SetCamera(Camera _camera)
    {
        if (m_currentCamera != null)
        {
            m_currentCamera.gameObject.SetActive(false);
        }
        m_currentCamera = _camera;
        m_currentCamera.gameObject.SetActive(true);
    }

    public IEnumerator LerpTo(Vector3 _position, Quaternion _rotation, float _fov, float _time)
    {
        m_previousSettings.Position = m_currentCamera.transform.position;
        m_previousSettings.Rotation = m_currentCamera.transform.rotation;
        m_previousSettings.FOV = m_currentCamera.fieldOfView;

        Vector3 startPosition = m_currentCamera.transform.position;
        Vector3 endPosition = _position;
        Quaternion startRotation = m_currentCamera.transform.rotation;
        Quaternion endRotation = _rotation;
        float startFOV = m_currentCamera.fieldOfView;
        float endFOV = _fov;

        float percent = 0f;
        float ratio = 1f / (_time > 0f ? _time : 0.1f);
        while (percent < 1f)
        {
            percent += Time.deltaTime * ratio;

            m_currentCamera.transform.position = Vector3.Lerp(startPosition, endPosition, HelperFunctions.EaseOutCubic(percent));
            m_currentCamera.transform.rotation = Quaternion.Lerp(startRotation, endRotation, HelperFunctions.EaseOutCubic(percent));
            m_currentCamera.fieldOfView = Mathf.Lerp(startFOV, endFOV, HelperFunctions.EaseOutCubic(percent));
            yield return null;
        }
    }

    public IEnumerator LerpToPreviousSettings(float _speed)
    {
        yield return LerpTo(m_previousSettings.Position, m_previousSettings.Rotation, m_previousSettings.FOV, _speed);
    }
}
