﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    // managers
    private CameraManager m_cameraManager;
    public CameraManager CameraManager { get { return m_cameraManager; } }

    private AudioManager m_audioManager;
    public AudioManager AudioManager { get { return m_audioManager; } }

    // state
    public GameStateID StartupState;
    public GameStateID CurrentStateID { get; private set; }
    public GameStateID PreviousStateID { get; private set; }
    private GameState m_currentGameState;
    private Dictionary<GameStateID, GameState> m_gameStateDictionary;

    public static event CustomDelegates.VoidDelegateGameStateID OnGameStateChanged;

    // etc
    public Canvas Canvas;


    protected virtual void Start()
    {
        // init managers
        m_cameraManager = GetComponent<CameraManager>();
        m_audioManager = GetComponent<AudioManager>();


        // init states
        m_gameStateDictionary = new Dictionary<GameStateID, GameState>();

        GameState[] gameStates = GetComponentsInChildren<GameState>();
        for (int i = 0; i < gameStates.Length; i++)
        {
            m_gameStateDictionary.Add(gameStates[i].StateID, gameStates[i]);
        }

        CurrentStateID = PreviousStateID = GameStateID.NONE;
        GoToGameState(StartupState);
    }

    public virtual void GoToGameState(GameStateID _stateID)
    {
        if (_stateID == CurrentStateID || !m_gameStateDictionary.ContainsKey(_stateID))
        {
            return;
        }

        // exit previous state
        if (m_currentGameState != null)
        {
            m_currentGameState.Exit();
        }

        PreviousStateID = CurrentStateID;
        CurrentStateID = _stateID;
        
        // enter new state
        m_currentGameState = m_gameStateDictionary[CurrentStateID];
        m_currentGameState.Enter();

        if (OnGameStateChanged != null)
            OnGameStateChanged.Invoke(CurrentStateID);
    }

    public GameState GetState(GameStateID _stateID)
    {
        if (m_gameStateDictionary.ContainsKey(_stateID))
        {
            return m_gameStateDictionary[_stateID];
        }

        return null;
    }

    public T GetStateWithType<T>(GameStateID _stateID)
    {
        if (m_gameStateDictionary.ContainsKey(_stateID) && m_gameStateDictionary[_stateID] is T)
        {
            return (T)System.Convert.ChangeType(m_gameStateDictionary[_stateID], typeof(T));
        }

        return default(T);
    }
}
