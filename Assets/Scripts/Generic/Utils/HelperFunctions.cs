﻿using UnityEngine;
using System;

public class HelperFunctions
{
    //Transfers a value from one scale to another scale, but clamps the return value within the second scale.
    public static float ScaleClamp(float value, float min, float max, float min2, float max2)
    {
        value = min2 + ((value - min) / (max - min)) * (max2 - min2);
        if (max2 > min2)
        {
            value = value < max2 ? value : max2;
            return value > min2 ? value : min2;
        }
        value = value < min2 ? value : min2;
        return value > max2 ? value : max2;
    }



    public static bool ApproxEquals(float floatA, float floatB, float margin = 0.2f)
    {
        return (floatA > floatB - margin) && (floatA < floatB + margin);
    }

    public static bool ApproxEquals(int intA, int intB, int margin = 1)
    {
        return (intA > intB - margin) && (intA < intB + margin);
    }    

    public static bool ApproxEquals(Vector3 _v1, Vector3 _v2, float _margin = 0.2f)
    {
        bool xCheck = ApproxEquals(_v1.x, _v2.x, _margin);
        bool yCheck = ApproxEquals(_v1.y, _v2.y, _margin);
        bool zCheck = ApproxEquals(_v1.z, _v2.z, _margin);

        return xCheck && yCheck && zCheck;
    }

    public static bool ApproxEqualsXZ(Vector3 _v1, Vector3 _v2, float _margin = 0.2f)
    {
        bool xCheck = ApproxEquals(_v1.x, _v2.x, _margin);
        bool zCheck = ApproxEquals(_v1.z, _v2.z, _margin);

        return xCheck && zCheck;
    }

    public static bool ApproxEquals(Quaternion _q1, Quaternion _q2, float _margin = 0.2f)
    {
        bool wCheck = ApproxEquals(_q1.w, _q2.w, _margin);
        bool xCheck = ApproxEquals(_q1.x, _q2.x, _margin);
        bool yCheck = ApproxEquals(_q1.y, _q2.y, _margin);
        bool zCheck = ApproxEquals(_q1.z, _q2.z, _margin);

        return wCheck && xCheck && yCheck && zCheck;
    }



    public static Vector3 Vector3XZ(Vector3 _v)
    {
        return new Vector3(_v.x, 0f, _v.z);
    }

    public static Vector3 Vector3FlipXZ(Vector3 _v)
    {
        return new Vector3(_v.z, _v.y, _v.x);
    }



    // ====================
    // easing functions
    //
    // Source: Warren Moore's AHEasing library
    // Original Author: Warren Moore
    // https://github.com/warrenm/AHEasing
    //
    public static float EaseInCubic(float _p)
    {
        return _p * _p * _p;
    }

    public static float EaseOutCubic(float _p)
    {
        return 1f - EaseInCubic(1f - _p);
    }

    public static float EaseInSine(float _p)
    {
        return Mathf.Sin((_p - 1) * (Mathf.PI * 2f)) + 1;
    }

    public static float EaseOutSine(float _p)
    {
        return Mathf.Sin(_p * (Mathf.PI * 2f));
    }

    public static float EaseInOutSine(float _p)
    {
        return 0.5f * (1f - Mathf.Cos(_p * Mathf.PI));
    }

    public static float EaseOutBack(float _p)
    {
        float f = (1f - _p);
        return 1f - (f * f * f - f * Mathf.Sin(f * Mathf.PI));
    }

    public static float EaseOutBounce(float _p)
    {
        if (_p < 0.36f)
        {
            return (121f * _p * _p) / 16f;
        }
        else if (_p < 0.73f)
        {
            return (9.075f * _p * _p) - (9.9f * _p) + 3.4f;
        }
        else if (_p < 0.9f)
        {
            return (12.1f * _p * _p) - (19.64f * _p) + 8.90f;
        }
        else
        {
            return (10.8f * _p * _p) - (20.52f * _p) + 10.72f;
        }
    }



    // source: Catlike Coding
    public static Vector3 QuadraticLerp(Vector3 _p0, Vector3 _p1, Vector3 _p2, float _t)
    {
        return Vector3.Lerp(Vector3.Lerp(_p0, _p1, _t), Vector3.Lerp(_p1, _p2, _t), _t);
    }
}
