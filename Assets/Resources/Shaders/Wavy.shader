﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/Wavy"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always



		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			float rand(float2 co)
			{
				return frac(cos(dot(co.xy, float2(10.0, 2.0))) * 300.0);
				//return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
			}
			

			float scaleClamp(float value, float min, float max, float min2, float max2)
			{
				value = min2 + ((value - min) / (max - min)) * (max2 - min2);
				if (max2 > min2)
				{
					value = value < max2 ? value : max2;
					return value > min2 ? value : min2;
				}
				value = value < min2 ? value : min2;
				return value > max2 ? value : max2;
			}

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				float2 med = i.uv;

				//if (i.uv.y > rand(i.uv))
				//{
					i.uv.y *= clamp(sin(rand(med) + _Time * 5.0f), 0.8f, 1.0f);
				//}

				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				if (i.uv.y < 0.1f)
				{
					//col = 1 - col;
				}
				//col = 1 - col;
				return col;
			}
			ENDCG
		}
	}	
}
