﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Pixelate"
{
	Properties
	{
		_MainTex("ScreenTex", 2D) = "white" {}
		_CellSize("Cell Size", Vector) = (0.0045, 0.0045, 0, 0)
	}

	SubShader
	{
		//Tags{ "RenderType" = "Opaque" }
		//LOD 200

		Pass
		{

		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};	

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
			float4 grabUV : POSITION1;
		};



		v2f vert(appdata v) 
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = v.uv;
			o.grabUV = ComputeGrabScreenPos(o.vertex);
			return o;
		}


		float4 _CellSize;
		sampler2D _MainTex;

		fixed4 frag(v2f i) : SV_Target
		{
			float2 steppedUV = i.grabUV.xy / i.grabUV.w;
			steppedUV /= _CellSize.xy;
			steppedUV = round(steppedUV);
			steppedUV *= _CellSize.xy;
			steppedUV.y = 1 - steppedUV.y;
			return tex2D(_MainTex, steppedUV);
		}

		ENDCG
	}
	}
}

