﻿Shader "Hidden/RGBDisplacement" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_ROffsetX("R Offset X", Range(0, 1)) = 0
		_ROffsetY("R Offset Y", Range(0, 1)) = 0
		_GOffsetX("G Offset X", Range(0, 1)) = 0
		_GOffsetY("G Offset Y", Range(0, 1)) = 0
		_BOffsetX("B Offset X", Range(0, 1)) = 0
		_BOffsetY("B Offset Y", Range(0, 1)) = 0
		_bwBlend("Black & White blend", Range(0, 1)) = 0
	}
		SubShader{

		Pass{
		CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"

		uniform sampler2D _MainTex;
	uniform float _bwBlend;

	float _ROffsetX;
	float _ROffsetY;
	float _GOffsetX;
	float _GOffsetY;
	float _BOffsetX;
	float _BOffsetY;

	float4 frag(v2f_img i) : COLOR
	{
		//float4 c = tex2D(_MainTex, i.uv);

		//float lum = c.r*.3 + c.g*.59 + c.b*.11;
		//float3 bw = float3( lum, lum, lum ); 

		//float4 result = c;
		//result.rgb = lerp(c.rgb, bw, _bwBlend);
		//return result;

		float2 redUV = i.uv;
		redUV.x += _ROffsetX;
		redUV.y += _ROffsetY;
		float2 blueUV = i.uv;
		blueUV.x += _BOffsetX;
		blueUV.y += _BOffsetY;
		float2 greenUV = i.uv;
		greenUV.x += _GOffsetX;
		greenUV.y += _GOffsetY;

		float3 colorR = tex2D(_MainTex, redUV);
		float3 colorG = tex2D(_MainTex, greenUV);
		float3 colorB = tex2D(_MainTex, blueUV);

		float4 returnColor;
		returnColor.r = colorR.r;
		returnColor.g = colorG.g;
		returnColor.b = colorB.b;
		returnColor.a = 1.0f;
		return returnColor;
	}
		ENDCG
	}
	}
}