# README #

hi, i'm wednesday

### What is this? ###

* this is **yn**, a game that I originally made in 48 hrs for Global Game Jam 2017. I hope to continue working on it here and there :)

### 3rd party libraries used ###

* [psx_retroshader](https://github.com/dsoft20/psx_retroshader)
* [pixelation shader](https://forum.unity3d.com/threads/making-a-local-pixelation-image-effect-shader.183210/)

### Software used ###

* Unity3d (code etc.)
* blender (models, animations)
* paint.net (UI elements, textures)
* bosca ceoli (music)
* bfxr + mouth noises distorted in audacity (sfx)